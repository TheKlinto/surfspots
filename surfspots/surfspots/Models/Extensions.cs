﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace surfspots.Models
{
    public static class Extensions
    {
        public static string USFormat(this double d)
        {
            string specifier = "G";
            return d.ToString(specifier, CultureInfo.InvariantCulture);
        }
    }
}
