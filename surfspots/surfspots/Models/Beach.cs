﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace surfspots.Models
{
    public class Beach
    {
        public string RatingDisplayed { get; set; }
        public string Name { get; set; }
        [Display(Name ="Samlet vurdering")]
        public int TotalRating { get; set; }
        public double Longtitude { get; set; }
        public double Lattitude { get; set; }
        public string Description { get; set; }
        public List<Rating> Ratings { get; set; }
        public WeatherData WeatherData { get; set; }
        public string Image { get; set; }
    }
}
