﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace surfspots.Models
{
	public class City
	{
		public int id { get; set; }
		public string name { get; set; }
		public string state { get; set; }
		public string country { get; set; }
		public virtual Coord coord { get; set; }
	}

	public class Coord
	{
		public int id { get; set; }
		public float lat { get; set; }
		public float lon { get; set; }
	}
}