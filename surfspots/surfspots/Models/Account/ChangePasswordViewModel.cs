﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace surfspots.Models.Account
{
    public class ChangePasswordViewModel
	{
		public ApplicationUser User { get; set; }

		[DataType(DataType.Password)]
		[Required]
		[Display(Name = "Nuværende adgngskode")]
		public string OldPassword { get; set; }
		[DataType(DataType.Password)]
		[Required]
		[Display(Name = "Ny adgangskode")]
		public string NewPassword { get; set; }
		[DataType(DataType.Password)]
		[Required]
		[Display(Name = "Bekræft ny adgangskode")]
		[Compare(nameof(NewPassword), ErrorMessage = "Adgangskoderne matcher ikke")]
		public string ConfirmNewPassword { get; set; }
	}
}
