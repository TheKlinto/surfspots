﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace surfspots.Models.Account
{
	public class ApplicationUser : IdentityUser
	{
		[Timestamp]
		public byte[] RowVersion { get; set; }
	}
}
