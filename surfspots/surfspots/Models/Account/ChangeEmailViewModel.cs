﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace surfspots.Models.Account
{
	public class ChangeEmailViewModel
	{
		public ApplicationUser User { get; set; }

		[Timestamp]
		public byte[] RowVersion { get; set; }
		[EmailAddress]
		[Display(Name = "Email Adresse")]
		public string EmailAdress { get; set; }
	}
}
