﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace surfspots.Models
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email Adresse")]
        public string EmailAdress { get; set; }
        [DataType(DataType.Password)]
        [Required]
        [Display(Name = "Adgangskode")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Required]
        [Display(Name = "Bekræft adgangskode")]
        [Compare("Password", ErrorMessage = "Adgangskoderne matcher ikke")]
        public string ConfirmPassword { get; set; }

    }
}