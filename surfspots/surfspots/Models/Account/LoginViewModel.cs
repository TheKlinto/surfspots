﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace surfspots.Models
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email Adresse")]
        public string EmailAdress { get; set; }
        [DataType(DataType.Password)]
        [Required]
        [Display(Name = "Adgangskode")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
