﻿using System;
using System.ComponentModel.DataAnnotations;

namespace surfspots.Models.QueryParametersHelpers
{
	public class QueryParameters
	{
		public int? MaxSize { get; private set; } = 500;

		[Display(Name = "side")]
		public int? Page { get; set; }

		private int? size = 100;

		[Display(Name = "størrelse")]
		public int? Size
		{
			get { return size; }
			set { size = Math.Min((byte)MaxSize, (byte)value); }
		}

		public string SortBy { get; set; } = "ID";

		private string sortOrder = "desc";

		public string SortOrder
		{
			get { return sortOrder; }
			set
			{
				if (value == "asc" || value == "desc")
				{
					sortOrder = value;
				}
			}
		}
	}
}