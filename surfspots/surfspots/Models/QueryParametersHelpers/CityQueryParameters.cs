﻿namespace surfspots.Models.QueryParametersHelpers
{
	public class CityQueryParameters : QueryParameters
	{
		public string Name { get; set; }

		public string Country { get; set; }
	}
}