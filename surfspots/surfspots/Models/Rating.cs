﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace surfspots.Models
{
    public class Rating
    {
        public int Stars { get; set; }
        public string Comment { get; set; }
        public int People { get; set; }
        public int WaveQuality { get; set; }
    }
}
