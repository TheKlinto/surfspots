﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace surfspots.Models
{
    public class WeatherData
    {
        [Display(Name ="Bølgehøjde")]
        public float WaveHeight { get; set;}
        [Display(Name = "Bølgeperiode")]
        public float WavePeriod { get; set; }
        [Display(Name = "Vindhastighed")]
        public float WindSpeed { get; set; }
        [Display(Name = "Vandtemperatur")]
        public float WaterTemperature { get; set; }
        [Display(Name = "Lufttemperatur")]
        public float AirTemperature { get; set; }
        [Display(Name = "Sidst opdateret")]
        public DateTime TimeStamp { get; set; }
    }
}
