﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using surfspots.Models;
using surfspots.Models.QueryParametersHelpers;

namespace surfspots.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;
		private readonly IHttpClientFactory _client;

		public HomeController(ILogger<HomeController> logger, IHttpClientFactory clientFactory)
		{
			_logger = logger;
			_client = clientFactory;
		}

		[Route("")]
		public IActionResult Index()
		{
			return View();
		}

		[HttpGet]
		public async Task<IActionResult> Locations([FromQuery] CityQueryParameters cityQueryParameters)
		{
			IQueryable<City> cities;

			using (HttpClient httpClient = new HttpClient())
			{
				//using (var response = await httpClient.GetAsync("https://localhost:44377/weatherforecast/getallcities"))
				//{
				//	string apiResponse = await response.Content.ReadAsStringAsync();
				//	cities = JsonConvert.DeserializeObject<List<City>>(apiResponse).AsQueryable();
				//}
				using (var response = await httpClient.GetAsync("http://api.surfspots.dk/weatherforecast/getallcities"))
				{
					string apiResponse = await response.Content.ReadAsStringAsync();
					cities = JsonConvert.DeserializeObject<List<City>>(apiResponse).AsQueryable();
				}
			}

			ViewBag.cityCount = cities.Count();
			ViewBag.page = cityQueryParameters.Page == null ? 1 : cityQueryParameters.Page;

			if (!string.IsNullOrWhiteSpace(cityQueryParameters.Country))
			{
				cities = cities.Where(p => p.country.ToLower().Contains(cityQueryParameters.Country.ToLower()));
			}

			if (!string.IsNullOrWhiteSpace(cityQueryParameters.Name))
			{
				cities = cities.Where(p => p.name.ToLower().Contains(cityQueryParameters.Name.ToLower()));
			}

			if (!string.IsNullOrWhiteSpace(cityQueryParameters.SortBy))
			{
				if (typeof(City).GetProperty(cityQueryParameters.SortBy) != null)
				{
					cities = cities.OrderByCustom(cityQueryParameters.SortBy, cityQueryParameters.SortOrder.ToLower());
				}
			}

			if (cityQueryParameters.Size != null && cityQueryParameters.Page != null)
			{
				cities = cities.Skip((int)cityQueryParameters.Size * ((int)cityQueryParameters.Page - 1)).Take((int)cityQueryParameters.Size);
			}
			else
			{
				cities = cities.Skip(10 * (1 - 1)).Take(10);
			}

			return View(cities.ToList());
		}

		[HttpPost]
		public IActionResult Locations(Beach beach)
		{
			return RedirectToAction("Beach");
		}

		public IActionResult Beach()
		{
			Beach b = new Beach
			{
				Name = "Assens Strand",
				Description = "Lorem Impsum",
				RatingDisplayed = "",
				TotalRating = 4,
				Lattitude = 55.207721,
				Longtitude = 9.986030,
				Ratings = new List<Rating>(),
				WeatherData = new WeatherData
				{
					AirTemperature = 22,
					WaterTemperature = 15,
					WaveHeight = 6,
					WavePeriod = 5,
					WindSpeed = 8,
					TimeStamp = DateTime.Now
				}
			};
			return View(b);
		}

		[HttpGet]
		public async Task<IActionResult> Search(string city)
		{
			// ændre url til host hjemmesidens url
			string json = await _client.CreateClient().GetStringAsync($"http://api.surfspots.dk/weatherforecast/{city}");
			//string json = await _client.CreateClient().GetStringAsync($"https://localhost:44377/weatherforecast/{city}");

			WeatherData weather = JsonConvert.DeserializeObject<WeatherData>(json);

			if (weather != null)
			{
				return View(weather);
			}

			return NotFound();
		}


		/*
		 * Muligvis ændre dette så kun 1 metode anvendes
		 * og der anvendes url id's i stedet for fulde url links
		 * til at finde de forskellige sider. Så kan man i stedet for sende en værdi med viewet
		 * som kan være indholdet på siden, i form af en klasse med titel og indhold som attributter.
		 */
		[Route("/om-surfspots")]
		public IActionResult AboutUs()
		{
			return View();
		}

		[Route("/om-gruppen")]
		public IActionResult AboutUsTeam()
		{
			return View();
		}

		[Route("/om-projektet")]
		public IActionResult AboutUsProject()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

        public IActionResult Contact()
        {
            return View();
        }

        [HttpGet]
        public IActionResult NewBeach()
        {
            return View(new Beach());
        }
        [HttpPost]
        public IActionResult NewBeach(Beach beach)
        {
            //Check for input
            //Implement feature to add to the list

            return Ok();

        }
    }
}
