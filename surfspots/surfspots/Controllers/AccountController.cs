﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using surfspots.Models;
using surfspots.Models.Account;

namespace surfspots.Controllers
{
	public class AccountController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;

		public AccountController(
			 SignInManager<ApplicationUser> signInManager,
			 UserManager<ApplicationUser> userManager
			 )
		{
			_signInManager = signInManager;
			_userManager = userManager;
		}

		[HttpGet]
		public IActionResult SignIn()
		{
			return View(new LoginViewModel());
		}

		public async Task<IActionResult> SignIn(LoginViewModel login, string returnUrl = null)
		{
			if (!ModelState.IsValid)
			{
				return View();
			}
			var result = await _signInManager.PasswordSignInAsync(
				 login.EmailAdress, login.Password,
				 false, false);

			if (!result.Succeeded)
			{
				ModelState.AddModelError("", "Login fejlede!");
				return View();
			}
			if (string.IsNullOrWhiteSpace(returnUrl))
				return RedirectToAction("Index", "Home");

			return Redirect(returnUrl);
		}

		[HttpGet]
		public IActionResult SignUp()
		{
			return View(new RegisterViewModel());
		}

		[HttpPost]
		public async Task<IActionResult> SignUp(RegisterViewModel register)
		{
			if (!ModelState.IsValid)
				return View(register);

			var newUser = new ApplicationUser
			{
				Email = register.EmailAdress,
				UserName = register.EmailAdress
			};

			var result = await _userManager.CreateAsync(newUser, register.Password);

			if (!result.Succeeded)
			{
				foreach (var error in result.Errors.Select(x => x.Description))
				{
					ModelState.AddModelError("", error);
				}
				return View();
			}

			return RedirectToAction("SignIn");
		}
		public async Task<IActionResult> SignOut(string returnUrl = null)
		{
			await _signInManager.SignOutAsync();

			if (string.IsNullOrWhiteSpace(returnUrl))
				return RedirectToAction("Index", "Home");

			return Redirect(returnUrl);
		}

		[HttpGet]
		public IActionResult ViewAccount()
		{
			return View();
		}

		[HttpGet]
		public async Task<IActionResult> ChangeEmail()
		{
			if (User.Identity.IsAuthenticated)
			{
				ChangeEmailViewModel editViewModel = new ChangeEmailViewModel()
				{
					User = await _userManager.FindByNameAsync(User.Identity.Name)
				};
				return View(editViewModel);
			}
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public async Task<IActionResult> ChangeEmail(ChangeEmailViewModel editedUser)
		{
			ApplicationUser user = _userManager.Users.FirstOrDefault(x => x.Id == editedUser.User.Id);

			user.RowVersion = editedUser.RowVersion;
			user.Email = editedUser.User.Email;
			user.UserName = editedUser.User.Email;

			//System.Threading.Thread.Sleep(5000); // Bliver brugt til at tjekke optimistisk offline lock 
			IdentityResult result = await _userManager.UpdateAsync(user);

			if (result.Succeeded)
			{
				await _signInManager.SignInAsync(user, true);
				return RedirectToAction("ViewAccount");
			}

			var dbUser = await _userManager.Users.AsNoTracking().SingleOrDefaultAsync(x => x.Id == user.Id);

			foreach (IdentityError error in result.Errors)
			{
				if (error.Code == "ConcurrencyFailure")
				{
					if (dbUser.Email != user.Email)
					{
						ModelState.AddModelError("Email", $"Email i Databasen: {dbUser.Email}");
					}

					ModelState.AddModelError(string.Empty,
					"Redigeringen blev annulleret, da den valgte bruger blev ændret af en anden i mellemtiden. " +
					"Ændringens værdier er nu vist herunder, som stammer fra databasen. " +
					"Hvis du alligevel vil redigere brugeren, skal du klikke på Gem igen, ellers klik på Fortryd.");
				}
				else
				{
					ModelState.AddModelError(string.Empty, error.Description);
				}
			}

			return View(editedUser);
		}

		[HttpGet]
		public async Task<IActionResult> ChangePassword()
		{
			if (User.Identity.IsAuthenticated)
			{
				ChangePasswordViewModel editViewModel = new ChangePasswordViewModel()
				{
					User = await _userManager.FindByNameAsync(User.Identity.Name)
				};
				return View(editViewModel);
			}
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public async Task<IActionResult> ChangePassword(ChangePasswordViewModel editedUser)
		{

			ApplicationUser user = await _userManager.FindByIdAsync(editedUser.User.Id);
			var token = await _userManager.GeneratePasswordResetTokenAsync(user);
			var result = await _userManager.ResetPasswordAsync(user, token, editedUser.ConfirmNewPassword);

			if (!ModelState.IsValid)
			{
				return View(editedUser);
			}

			if (!result.Succeeded)
			{
				foreach (var error in result.Errors.ToList())
				{
					ModelState.AddModelError(error.Code, error.Description);
				}

				return View(editedUser);
			}

			return RedirectToAction("ViewAccount");
		}


		//Not implementet
		[HttpGet]
		public IActionResult PaymentDetails()
		{
			return View();
		}

		public IActionResult ViewAccountPartial(string partial = null)
		{
			return PartialView();
		}
	}
}
