﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using APISurfspots.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using APISurfspots.Models.OpenWeather;
using APISurfspots.Models.MetNo;
using System.Xml.Serialization;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace APISurfspots.Controllers
{
	[ApiController]
	[Route("[controller]")]
	
	public class WeatherForecastController : ControllerBase
	{
		private ILogger<WeatherForecastController> _logger { get; set; }
		private CityDbContext _db { get; set; }
		private IHttpClientFactory _factory { get; set; }
		private const string apiKey = "7e7c1d12e20aaee2dfb7ef4d5d5c27c4";

		public WeatherForecastController(ILogger<WeatherForecastController> logger, IHttpClientFactory factory, CityDbContext cityDbContext)
		{
			_logger = logger;
			_factory = factory;
			_db = cityDbContext;
		}

		/// <summary>
		/// Returns all cities with seperate weatherforecasts from the database
		/// </summary>
		/// <response code="200">Returns all cities found</response>
		[Produces("text/json")]
		[HttpGet("getallcities")]
		public async Task<ActionResult<IEnumerable<City>>> GetAllCities()
		{
			return await _db.Cities.ToListAsync();
		}

		/// <summary>
		/// Returns the 10 first cities found coresponding to the parameter
		/// </summary>
		/// <response code="200">Returns the name(s) of the city(s) found</response>
		/// <response code="400">If no cities are found</response>
		[HttpGet("search")]
		public async Task<IActionResult> Search()
		{
			try
			{
				string term = HttpContext.Request.Query["term"].ToString();
				var cityName = await _db.Cities.Where(p => p.name.Contains(term)).Select(p => p.name).Take(10).ToListAsync();
				return Ok(cityName);
			}
			catch
			{
				return BadRequest();
			}
		}

		[HttpGet]
		[Route("{id:int}")]
		public async Task<IActionResult> GetWeather(int id)
		{
			City city = await _db.Cities.Include(x => x.coord).FirstOrDefaultAsync(x => x.id == id);

			if (city != null)
			{
				try
				{
					string json = await _factory.CreateClient().GetStringAsync($"https://api.openweathermap.org/data/2.5/onecall?lat={city.coord.lat}&lon={city.coord.lon}&lang=da&exclude=minutely,hourly,allerts&units=metric&appid={apiKey}");
					WeatherData openWeather = JsonConvert.DeserializeObject<WeatherData>(json);

					DateTimeOffset dt = DateTimeOffset.UnixEpoch.AddSeconds(openWeather.current.dt);
					DateTime current = dt.DateTime;

					WeatherResult weatherResult = new WeatherResult()
					{
						AirTemperature = openWeather.current.temp,
						WindSpeed = openWeather.current.wind_speed,
						TimeStamp = current
					};

					// failsafe for når lokationer ikke er i havet.
					try
					{
						string apiRequestMetNo = $"https://api.met.no/weatherapi/oceanforecast/0.9/.xml?lat={city.coord.lat}&lon={city.coord.lon}";
						string xmlres = await _factory.CreateClient().GetStringAsync(apiRequestMetNo);
						XmlSerializer serializer = new XmlSerializer(typeof(Forecasts));
						Forecasts weather2;
						using (StringReader sr = new StringReader(xmlres))
						{
							weather2 = (Forecasts)serializer.Deserialize(sr);
						}

						weatherResult.WaterTemperature = (float)weather2.forecast[0].OceanForecast.seaTemperature.Value;
						weatherResult.WaveHeight = (float)weather2.forecast[0].OceanForecast.significantTotalWaveHeight.Value;
					}
                    catch (Exception)
                    {
						return Ok(weatherResult);
					}

					return Ok(weatherResult);
				}
				catch (Exception)
				{
					return NotFound();
				}
			}

			return NotFound();
		}

		[HttpGet]
		[Route("{name}")]
		// Ustabil, danske bogstaver som ÆØÅ virker på nogens requests
		public async Task<IActionResult> GetWeather(string name)
		{
			City city = await _db.Cities.Include(x => x.coord).FirstOrDefaultAsync(x => x.name == name);

			if (city != null)
			{
				try
				{
					string json = await _factory.CreateClient().GetStringAsync($"https://api.openweathermap.org/data/2.5/onecall?lat={city.coord.lat}&lon={city.coord.lon}&lang=da&exclude=minutely,hourly,allerts&units=metric&appid={apiKey}");
					WeatherData openWeather = JsonConvert.DeserializeObject<WeatherData>(json);

					DateTimeOffset dt = DateTimeOffset.UnixEpoch.AddSeconds(openWeather.current.dt);
					DateTime current = dt.DateTime;

					WeatherResult weatherResult = new WeatherResult()
					{
						AirTemperature = openWeather.current.temp,
						WindSpeed = openWeather.current.wind_speed,
						TimeStamp = current

					};

					
					try
					{
						string apiRequestMetNo = $"https://api.met.no/weatherapi/oceanforecast/0.9/.xml?lat={city.coord.lat}&lon={city.coord.lon}";
						string xmlres = await _factory.CreateClient().GetStringAsync(apiRequestMetNo);
						XmlSerializer serializer = new XmlSerializer(typeof(Forecasts));
						Forecasts weather2;
						using (StringReader sr = new StringReader(xmlres))
						{
							weather2 = (Forecasts)serializer.Deserialize(sr);
						}

						weatherResult.WaterTemperature = (float)weather2.forecast[0].OceanForecast.seaTemperature.Value;
						weatherResult.WaveHeight = (float)weather2.forecast[0].OceanForecast.significantTotalWaveHeight.Value;
					}
					catch (Exception)
					{
						return Ok(weatherResult);
					}

					return Ok(weatherResult);
				}
				catch (Exception)
				{
					return NotFound();
				}
			}

			return NotFound();
		}

		//Security Flaw
		//Debug tool
		//[Authorize(AuthenticationSchemes = "Bearer")]
		[HttpPost]
		public async Task<ActionResult<City>> PostWeatherForecast([FromBody] City city)
		{
			_db.Cities.Add(city);
			await _db.SaveChangesAsync();

			return CreatedAtAction("GetWeather", new { id = city.id }, city);
		}

		//[Authorize(AuthenticationSchemes = "Bearer")]
		[HttpPut("{id}")]
		public async Task<IActionResult> PutWeatherForecast(int id, [FromBody] City city)
		{
			if (id != city.id)
			{
				return BadRequest();
			}

			_db.Entry(city).State = EntityState.Modified;

			try
			{
				await _db.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (await _db.Cities.FindAsync(id) is null)
				{
					return NotFound();
				}

				return StatusCode(409);
			}

			return Ok();
		}

		//[Authorize(AuthenticationSchemes = "Bearer")]
		[HttpDelete("{id}")]
		public async Task<ActionResult<City>> DeleteWeatherForecast(int id)
		{
			City cityToDelete = await _db.Cities.FindAsync(id);

			if (cityToDelete is null)
			{
				return NotFound();
			}

			_db.Cities.Remove(cityToDelete);
			await _db.SaveChangesAsync();

			return cityToDelete;
		}

		[HttpGet("waveperiod")]
		public async Task<IActionResult> GetWavePeriod([FromQuery] float lon, float lat)
		{
			// api url: https://metoc.fcoo.dk/api/index
			// This method takes the image output from FCOO's Api and returns it
			// test url: https://localhost:44377/weatherforecast/waveperiod?lat=55.724403&lon=10.053320

			//float to string conversion (no comma)
			string longtitude = lon.ToString(System.Globalization.CultureInfo.InvariantCulture);
			string latitude = lat.ToString(System.Globalization.CultureInfo.InvariantCulture);

			string apiRequest = $"http://metoc.fcoo.dk/webapi/plot/timeseries?p=Wave_period&x={longtitude}&y={latitude}";
			byte[] response = await _factory.CreateClient().GetByteArrayAsync(apiRequest);
			// this code takes the byte response and converts it to the correct file type (defined by "image/type")
			return File(response, "image/jpeg");
		}
	}
}