﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APISurfspots.Models.OpenWeather
{
    //Link to parameters: https://openweathermap.org/api/one-call-api#parameter

    public class WeatherData
    {
        public float lat { get; set; }
        public float lon { get; set; }
        public Current current { get; set; }
        public Daily[] daily { get; set; }
    }
}
