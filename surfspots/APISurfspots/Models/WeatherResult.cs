﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APISurfspots.Models
{
    public class WeatherResult
    {
        public float WaveHeight { get; set; }
        public float WavePeriod { get; set; }
        public float WindSpeed { get; set; }
        public float WaterTemperature { get; set; }
        public float AirTemperature { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
