﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APISurfspots.Models.MetNo
{


    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://wdb.met.no/wdbxml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://wdb.met.no/wdbxml", IsNullable = false)]
    public partial class Forecasts
    {
        private ForecastsForecast[] forecastField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("forecast")]
        public ForecastsForecast[] forecast
        {
            get
            {
                return this.forecastField;
            }
            set
            {
                this.forecastField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.opengis.net/gml")]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://wdb.met.no/wdbxml")]
    public partial class ForecastsForecast
    {

        private OceanForecast oceanForecastField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://api.met.no")]
        public OceanForecast OceanForecast
        {
            get
            {
                return this.oceanForecastField;
            }
            set
            {
                this.oceanForecastField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://api.met.no")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://api.met.no", IsNullable = false)]
    public partial class OceanForecast
    {

        private validTime validTimeField;

        private meanTotalWaveDirection meanTotalWaveDirectionField;

        private significantTotalWaveHeight significantTotalWaveHeightField;

        private seaCurrentDirection seaCurrentDirectionField;

        private seaCurrentSpeed seaCurrentSpeedField;

        private seaTemperature seaTemperatureField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://wdb.met.no/wdbxml")]
        public validTime validTime
        {
            get
            {
                return this.validTimeField;
            }
            set
            {
                this.validTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://wdb.met.no/wdbxml")]
        public meanTotalWaveDirection meanTotalWaveDirection
        {
            get
            {
                return this.meanTotalWaveDirectionField;
            }
            set
            {
                this.meanTotalWaveDirectionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://wdb.met.no/wdbxml")]
        public significantTotalWaveHeight significantTotalWaveHeight
        {
            get
            {
                return this.significantTotalWaveHeightField;
            }
            set
            {
                this.significantTotalWaveHeightField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://wdb.met.no/wdbxml")]
        public seaCurrentDirection seaCurrentDirection
        {
            get
            {
                return this.seaCurrentDirectionField;
            }
            set
            {
                this.seaCurrentDirectionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://wdb.met.no/wdbxml")]
        public seaCurrentSpeed seaCurrentSpeed
        {
            get
            {
                return this.seaCurrentSpeedField;
            }
            set
            {
                this.seaCurrentSpeedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://wdb.met.no/wdbxml")]
        public seaTemperature seaTemperature
        {
            get
            {
                return this.seaTemperatureField;
            }
            set
            {
                this.seaTemperatureField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.opengis.net/gml")]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://wdb.met.no/wdbxml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://wdb.met.no/wdbxml", IsNullable = false)]
    public partial class validTime
    {

        private TimePeriod timePeriodField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public TimePeriod TimePeriod
        {
            get
            {
                return this.timePeriodField;
            }
            set
            {
                this.timePeriodField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class TimePeriod
    {

        private System.DateTime beginField;

        private System.DateTime endField;

        private string idField;

        /// <remarks/>
        public System.DateTime begin
        {
            get
            {
                return this.beginField;
            }
            set
            {
                this.beginField = value;
            }
        }

        /// <remarks/>
        public System.DateTime end
        {
            get
            {
                return this.endField;
            }
            set
            {
                this.endField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified)]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://wdb.met.no/wdbxml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://wdb.met.no/wdbxml", IsNullable = false)]
    public partial class meanTotalWaveDirection
    {

        private string uomField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://wdb.met.no/wdbxml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://wdb.met.no/wdbxml", IsNullable = false)]
    public partial class significantTotalWaveHeight
    {

        private string uomField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://wdb.met.no/wdbxml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://wdb.met.no/wdbxml", IsNullable = false)]
    public partial class seaCurrentDirection
    {

        private string uomField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://wdb.met.no/wdbxml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://wdb.met.no/wdbxml", IsNullable = false)]
    public partial class seaCurrentSpeed
    {

        private string uomField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://wdb.met.no/wdbxml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://wdb.met.no/wdbxml", IsNullable = false)]
    public partial class seaTemperature
    {

        private string uomField;

        private decimal valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string uom
        {
            get
            {
                return this.uomField;
            }
            set
            {
                this.uomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public decimal Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }



}
